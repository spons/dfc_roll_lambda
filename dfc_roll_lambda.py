import json

import rollprob.dfc as dfc


# def validate_min_max(d, key, minv, maxv):
#     if key not in d:
#         return 'Missing {} parameter'.format(key)
#     if d[key] < minv or d[key] > maxv:
#         return '{} cannot be lower than {} or higher than {}' \
#             .format(key, minv, maxv)
#     return None


# def validate_body(body):
#     errors = [validate_min_max(body, 'pd', 0, 12),
#               validate_min_max(body, 'armor', 1, 6)]
#     if 'weapons' not in body:
#         errors.append('Missing {} parameter'.format('weapons'))
#     else:
#         for weapon in body['weapons']:
#             if 'special' not in weapon:
#                 errors.append('Missing {} parameter'.format('special'))
#             errors.append(validate_min_max(weapon, 'lock', 1, 6))
#             errors.append(validate_min_max(weapon, 'attack', 1, 16))
#             errors.append(validate_min_max(weapon, 'dmg', 1, 6))
#
#     errors = list(set(errors))
#     try:
#         errors.remove(None)
#     except ValueError:
#         pass
#     if errors:
#         error_msg = '\n'.join(errors)
#         raise ValueError(error_msg)


def format_response(tuple_list):
    body = []
    for t in tuple_list:
        body.append({'dmg': t.dmg, 'p': round(float(t.p * 100), 4)})

    response = {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},
        'body': json.dumps(body)
    }

    return response


def lambda_handler(event, context):
    body = json.loads(event['body'])
    # I'm using the API JSON schema validation instead
    # validate_body(body)

    results = []
    for weapon in body['weapons']:
        burnthrough = 'burnthrough' in weapon['special']

        weapon_results = dfc.prob_saved_damage(
            dmg=dfc.prob_damage(
                hits=dfc.prob_hit(
                    lock=weapon['lock'],
                    num_attacks=weapon['attack'],
                    burnthrough=burnthrough),
                dmg=weapon['dmg']),
            armor=body['armor'])

        if 'cc' in weapon['special']:
            weapon_results = dfc.prob_damage_point_defence(
                dmg=weapon_results,
                pd=body['pd'],
            )
        results.append(weapon_results)

    if len(results) > 1:
        results = dfc.sum_damage(results)
    else:
        results, = results

    return format_response(results)
