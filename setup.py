import os
from setuptools import setup, find_packages

VERSION = '0.1.0'

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'LICENSE')) as f:
    LICENSE = f.read()

requires = [
    'rollprob == 0.3.1',
    ]

externals = [
    'git+git@gitlab.com:spons/rollprob.git@0.3.1#egg=rollprob-0.3.1',
]

setup(
    name='dfc_roll_lambda',
    version=VERSION,
    description='AWS Lambda function to simulate and attack roll for '
    'Dropfleet Commander.',
    long_description=README,
    classifiers=[
        'Programming Language :: Python',
        ],
    author='Sergi Pons Freixes',
    author_email='sergi@cub3.net',
    url='https://gitlab.com/spons/dfc_roll_lambda',
    keywords='dice roll probability dropfleet commander aws amazon lambda',
    license=LICENSE,
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=requires,
    dependency_links=externals,
)
