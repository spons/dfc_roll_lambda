from functools import reduce
from fractions import Fraction
from math import factorial
from operator import mul
from typing import Iterable


def prob_roll_lt(threshold: int, sides: int=6) -> Fraction:
    """Probability of rolling less than a threshold."""
    die = range(1, sides + 1)
    if threshold > sides:
        p = Fraction(1)
    else:
        p = Fraction(die.index(threshold), sides)
    return p


def prob_roll_ge(threshold: int, sides: int=6) -> Fraction:
    """Probability of rolling greater or equal than a threshold."""
    return 1 - prob_roll_lt(threshold, sides)


def prob_combination_factor(comb_count: Iterable[int]) -> int:
    """
    Counts combinations of elements. For example, if we have balls of 3 colors
    (green, redb, blue), how many combinations we have when picking 6 balls so
    that we pick 3 greens, 2 reds, and 1 blue. Generally: For n objects with k
    types, and n_i objects of type i we have the number of possible permutations
    is n! / (n_1! ... n_k!). See:
    https://math.stackexchange.com/questions/1049950/calculating-the-probability-of-a-combination-with-repetition

    :param comb_count: how many picks of each element
    :returns: counts
    """
    comb_len = sum(comb_count)
    denominator = reduce(mul, map(factorial, comb_count))
    count = factorial(comb_len) / denominator
    return int(count)
