from collections import Iterable, namedtuple
from fractions import Fraction
from itertools import combinations_with_replacement, groupby
from operator import add, attrgetter, mul
from typing import Any, Callable, List, Tuple

from rollprob.base import prob_combination_factor, prob_roll_ge, prob_roll_lt


HitSample = namedtuple('HitSample', 'p, miss, hit, crit')
HitDmgSample = namedtuple('HitDmgSample', 'p hit_dmg crit_dmg')
DmgSample = namedtuple('DmgSample', 'p dmg')


def combine_tuples(samples: List[Any], keys: List[str]) -> List[Any]:
    """
    Merges a list of named tuples based on the common value of some fields. The
    fields not used to condition the merge are added together. Basically, what
    is known as 'group by' and 'sum' on SQL or Pandas.

    :param samples: list of named tuples
    :param keys: what keys to use for merging
    :return: list of merged named tuples
    """
    all_keys = samples[0]._fields
    tuple_type = samples[0].__class__
    sum_keys = list(all_keys)
    for key in keys:
        sum_keys.remove(key)
    samples = sorted(samples, key=attrgetter(*keys))
    results = []
    for keys_values, group in groupby(samples, key=attrgetter(*keys)):
        sum_sample = {}
        if isinstance(keys_values, Iterable):
            for key, value in zip(keys, keys_values):
                sum_sample[key] = value
        else:
            sum_sample[keys[0]] = keys_values
        for group_sample in group:
            for key in sum_keys:
                if key in sum_sample:
                    sum_sample[key] += getattr(group_sample, key)
                else:
                    sum_sample[key] = getattr(group_sample, key)
        results.append(tuple_type(*[sum_sample[key] for key in all_keys]))
    return results


def prob_roll_thresholds(lock: int, miss: int, hit: int, crit: int,
                         crit_diff: int=2) -> Fraction:
    """
    Calculates the probabiliy o rolling a scpecific combination of misses, hits
    and crits on an attack roll.

    :param lock: Lock number of the weapon
    :param miss: Number of misses rolled
    :param hit: Number of hits rolled
    :param crit: Number of crits rolled
    :param crit_diff: What difference over the lock number makes a hit become a
    crit. The default value for all weapons is 2, but it can be modified by
    special rules.
    :return: Probability
    """
    p_one_crit = prob_roll_ge(lock + crit_diff)
    p_miss = prob_roll_lt(lock) ** miss
    p_hit = (prob_roll_ge(lock) - p_one_crit) ** hit
    p_crit = p_one_crit ** crit
    p = p_miss * p_hit * p_crit
    return p


def prob_roll_saves(armor: int, saved: int, unsaved: int) -> Fraction:
    """
    Calculates the probability of rolling a specific number of saves.

    :param armor: Armor (saving roll threshold)
    :param saved: Number or successful saves rolled
    :param unsaved: Number of failed saves rolled
    :return: Probability
    """
    p_saved = prob_roll_ge(armor) ** saved
    p_unsaved = prob_roll_lt(armor) ** unsaved
    p = p_saved * p_unsaved
    return p


def prob_combination(comb: Tuple[str, ...], num_attacks: int, lock: int,
                     crit_diff: int) -> Tuple[Fraction, int, int, int]:
    """
    For a specific result of an attack roll, and the weapon stats, calculates
    the probability of rolling this specific combination of misses-hits-crits.

    :param comb: Tuple with all the possible results we can roll for that
    weapon, for a single roll. For example, a weapon with 2 attack dice can
    theoretically roll 2 misses, 2 hits, 2 crits, or any combination of the
    former (like 1 hit and 1 crit). This tuple represents this roll as a series
    of strings. For example: ('miss', 'miss') or ('crit', 'hit').

    :param num_attacks: Attack value of the weapon
    :param lock: Lock value of the weapon
    :param crit_diff: What difference over the lock number makes a hit become a
    crit
    :return: Number of misses, hits and crits and the probability of rolling
    this specific combination.
    """
    miss = comb.count('miss')
    hit = comb.count('hit')
    crit = num_attacks - miss - hit
    # https://math.stackexchange.com/questions/1049950/calculating-the-probability-of-a-combination-with-repetition
    p = prob_roll_thresholds(lock, miss, hit, crit, crit_diff) * \
        prob_combination_factor((miss, hit, crit))
    return p, miss, hit, crit


def prob_hit(lock: int, num_attacks: int=1, crit_diff: int=2,
             burnthrough: bool=False, limit: int=6) -> List[HitSample]:
    """
    Calculates the probabilities of all the possible results (number of misses,
    hits and crits) of an attack roll.

    :param lock: Weapon's lock stat
    :param num_attacks: Weapon's attack stat
    :param crit_diff: What difference over the lock number makes a hit become a
    crit (if no special rules apply, the default is 2)
    :param burnthrough: If the weapon has the Burnthrough special rule
    :param limit: Burnthrough limit
    :return: List of all the possible combinations of number of misses, hits
    and crits and their probability
    """
    results: List[HitSample] = []
    combinations = combinations_with_replacement(
        ['miss', 'hit', 'crit'], r=num_attacks)
    for comb in combinations:
        p, miss, hit, crit = \
            prob_combination(comb, num_attacks, lock, crit_diff)
        if p:
            if burnthrough and (hit or crit):
                results = results + \
                          prob_burnthrough_chain(lock, p, miss, hit, crit,
                                                 limit, crit_diff)
            else:
                results.append(HitSample(p, miss, hit, crit))
    return results


def prob_burnthrough_chain(lock: int, prev_p: Fraction, prev_miss: int,
                           prev_hit: int, prev_crit: int, limit: int,
                           crit_diff: int) -> List[HitSample]:
    """
    Calculates the probabilities of all the possible results (number of misses,
    hits and crits) for a Burnthrough attack roll.

    :param lock: Weapon's lock stat
    :param prev_p: Previous probability of rolling this attack
    :param prev_miss: Number of previous misses rolled
    :param prev_hit: Number of previous hits rolled
    :param prev_crit: Number of previous crits rolled
    :param limit: Burnthough limit
    :param crit_diff: Difference bewteen hit and crit
    :return: List of all the possible combinations of number of misses, hits
    and crits and their probability
    """
    results: List[HitSample] = []
    num_attacks = prev_hit + prev_crit
    if prev_crit:
        crit_diff = 0
    combinations = combinations_with_replacement(
        ['miss', 'hit', 'crit'], r=num_attacks)
    for comb in combinations:
        p, miss, hit, crit = \
            prob_combination(comb, num_attacks, lock, crit_diff)
        if p:
            if hit or crit:
                if hit + crit + num_attacks == limit:
                    results.append(HitSample(prev_p * p, prev_miss + miss,
                                             prev_hit + hit, prev_crit + crit))
                # This case should never happen because we should hit first
                # hit + crit + num_attacks == limit
                # I leave it for reference.
                # elif hit + crit + num_attacks > limit:
                #     if crit + num_attacks > limit:
                #         crit = limit - num_attacks
                #         hit = 0
                #     else:
                #         hit = limit - num_attacks - crit
                #     results.append((prev_p * p, prev_miss + miss,
                #                     prev_hit + hit, prev_crit + crit))
                else:
                    next_chain_results = \
                        prob_burnthrough_chain(lock, p, miss, hit, crit,
                                               limit - num_attacks,
                                               crit_diff)
                    for p, miss, hit, crit in next_chain_results:
                        results.append(
                            HitSample(prev_p * p, prev_miss + miss,
                                      prev_hit + hit, prev_crit + crit))
            else:
                results.append(HitSample(prev_p * p, prev_miss + miss,
                                         prev_hit + hit, prev_crit + crit))
    return results


def prob_damage(hits: List[HitSample], dmg: int=1) -> List[HitDmgSample]:
    """
    Calculates the probability of rolling certain damage combinations based on
    some attack rolls combinations.

    :param hits: List of all the combinations of number of misses, hits
    and crits and their probability
    :param dmg: Weapon's damage stat
    :return: List of all the combinations of normal damage and critical damage
    and their probability
    """
    return [HitDmgSample(p=sample.p, hit_dmg=sample.hit * dmg,
                         crit_dmg=sample.crit * dmg)
            for sample in hits]


def prob_roll_pd(threshold: int, fail: int, success: int) -> Fraction:
    """
    Calculates the probability of rolling a specific combination of failed and
    successes on a Point Defence roll.

    :param threshold: PD value
    :param fail: Number of failed rolls
    :param success: Number of successful rolls
    :return: Probability
    """
    p_success = prob_roll_ge(threshold) ** success
    p_fail = prob_roll_lt(threshold) ** fail
    p = p_success * p_fail
    return p


def prob_pd_combination(comb: Tuple[str, ...], pd: int, threshold: int) -> \
        Tuple[Fraction, int, int]:
    """
    For a specific result of a Point Defence roll, and the PD stats, calculates
    the probability of rolling this specific combination of failed-saved rolls

    :param comb: Tuple with all the possible results we can roll for that
    PD, for a single roll. For example, a PD of 2 dice can theoretically roll
    2 fails, 2 successes, or 1 fail and 1 success. This tuple represents this
    roll as a series of strings. For example: ('fail', 'fail') or
    ('fail', 'success').
    :param pd: Point Defence value
    :param threshold: Threshold to roll equal or over to count as success. The
    default is 5 but can be modified by special rules.
    :return: Number of fails and successes and the probability of rolling this
    specific combination.
    """
    fail = comb.count('fail')
    success = pd - fail
    # See prob_combination
    p = prob_roll_pd(threshold, fail, success) * \
        prob_combination_factor((fail, success))
    return p, fail, success


def prob_damage_point_defence(dmg: List[HitDmgSample], pd: int,
                              threshold: int=5, crits_first: bool=True) \
        -> List[HitDmgSample]:
    """
    For a list of tuples of damages and probabilities, calculates the resulting
    list of applying Point Defence saves to them.

    :param dmg: List of tuples with normal damages, critical damages and their
    probabilities
    :param pd: Point Defence value
    :param threshold: Threshold of the PD. Tipically 5+, except when special
    rules apply
    :param crits_first: Decide if we want to cancel critical damage before
    cancelling normal damage. Normal damage takes 1 success to cancel, critical
    damage takes 2 successes to cancel.
    :return: List of tuples of normal and citical damages and probabilities
    """
    results: List[Tuple] = []
    for row in dmg:
        if row.hit_dmg == 0 and row.crit_dmg == 0:
            results.append(HitDmgSample(p=row.p, hit_dmg=row.hit_dmg,
                                        crit_dmg=row.crit_dmg))
        else:
            combinations = \
                combinations_with_replacement(['fail', 'success'], r=pd)
            for comb in combinations:
                p, fail, success = prob_pd_combination(comb, pd, threshold)
                if p:
                    remaining_crits = row.crit_dmg
                    remaining_hits = row.hit_dmg
                    if crits_first:
                        saved_crits = int(success / 2)
                        if saved_crits > remaining_crits:
                            saved_crits = remaining_crits
                        remaining_crits = remaining_crits - saved_crits
                        saved_hits = success - (saved_crits * 2)
                        if saved_hits > remaining_hits:
                            saved_hits = remaining_hits
                        remaining_hits = remaining_hits - saved_hits
                    else:
                        saved_hits = success
                        if saved_hits > remaining_hits:
                            saved_hits = remaining_hits
                        remaining_hits = remaining_hits - saved_hits
                        saved_crits = int((success - saved_hits) / 2)
                        if saved_crits > remaining_crits:
                            saved_crits = remaining_crits
                        remaining_crits = remaining_crits - saved_crits
                        # Check if we will we wasting a PD success for a hit
                        # instead of a crit
                        if success - saved_hits - (2 * saved_crits) == 1 and \
                           remaining_crits > 0 and saved_hits > 0:
                            remaining_hits += 1
                            saved_hits -= 1
                            remaining_crits -= 1
                            saved_crits += 1
                    results.append(HitDmgSample(p=p * row.p,
                                                hit_dmg=remaining_hits,
                                                crit_dmg=remaining_crits))

    return combine_tuples(results, keys=['hit_dmg', 'crit_dmg'],)


def prob_saved_damage(dmg: List[HitDmgSample], armor: int=4) -> List[DmgSample]:
    """
    For a list of tuples of damages and probabilities, calculates the resulting
    list of applying Armor Saves to them.

    :param dmg: List of tuples with normal damages, critical damages and their
    probabilities
    :param armor: Armor value
    :return: List of tuples of total damage and probability
    """
    results = []
    for sample in dmg:
        if sample.hit_dmg == 0:
            results.append(DmgSample(p=sample.p, dmg=sample.crit_dmg))
        else:
            num_saves = sample.hit_dmg
            combinations = combinations_with_replacement(
                ['saved', 'unsaved'], r=num_saves)
            for comb in combinations:
                saved = comb.count('saved')
                unsaved = num_saves - saved
                p = prob_roll_saves(armor, saved, unsaved) * \
                    prob_combination_factor((saved, unsaved))
                if p:
                    results.append(DmgSample(p=p * sample.p,
                                             dmg=unsaved + sample.crit_dmg))
    return combine_tuples(results, keys=['dmg'])


def np_ufunc_outer(op: Callable, a: Iterable, b: Iterable) -> List:
    """
    Replicates the numpy.ufunc.outer on pure python, and applied to iterables
    (lists, tuples, etc.) instead of umpy arrays. See
    https://docs.scipy.org/doc/numpy/reference/generated/numpy.ufunc.outer.html#numpy.ufunc.outer

    :param op: Function to apply. Tipically operator.odd or operator.mul
    :param a: First iterable
    :param b: Second iterable
    :return: Resulting iterable
    """
    result = []
    for a_element in a:
        if isinstance(a_element, Iterable):
            row = np_ufunc_outer(op, a_element, b)
        else:
            row = []
            for b_element in b:
                if isinstance(b_element, Iterable):
                    row.append(np_ufunc_outer(op, [a_element], b_element))
                else:
                    row.append(op(a_element, b_element))
        result.append(row)
    return result


def multi_ufunc_outer(op: Callable, namedtuples_list: List[List],
                      field: str= 'dmg') -> List:
    """
    Applies a replica of numpy.ufunc.outer (see dfc.np_ufunc_outer) to specific
    fields of a list of named tuples.

    :param op: Function to apply. Tipically operator.odd or operator.mul
    :param namedtuples_list: List of named tuples
    :param field: name of the field on the named tuples where to apply the
    operator. All the tuples must have this field.
    :return: Resulting iterable
    """

    if len(namedtuples_list) == 2:
        a = [getattr(row, field) for row in namedtuples_list[0]]
        b = [getattr(row, field) for row in namedtuples_list[1]]
    else:
        a = multi_ufunc_outer(op, namedtuples_list[:-1], field)
        b = [getattr(row, field) for row in namedtuples_list[-1]]
    return np_ufunc_outer(op, a, b)


def flatten(l: Iterable) -> Any:
    """Flattens nested iterables, returns an iterator"""
    for el in l:
        if isinstance(el, Iterable):
            yield from flatten(el)
        else:
            yield el


def sum_damage(damages_list: List[List]) -> List[DmgSample]:
    """
    Adds together the results of multiple attack and damage rolls.

    :param damages_list: List of lists of tuples of total damage and probability
    :return: List of tuples of total damage and probability
    """
    # https://stackoverflow.com/questions/41139104/probability-functions-convolution-in-python
    values = flatten(multi_ufunc_outer(add, damages_list, 'dmg'))
    probs = flatten(multi_ufunc_outer(mul, damages_list, 'p'))
    result = [DmgSample(p, dmg) for p, dmg in zip(probs, values)]
    result = combine_tuples(result, keys=['dmg'])
    return result
